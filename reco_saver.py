import ROOT
from array import array


m_t = 172.6*1000 #MeV
m_W = 80.27*1000 
sigma_t = 2.787*1000
sigma_W = 4.372*1000


# Helper Functions 
#region

def theta(eta):
    return 2*np.arctan(np.exp(-eta))

def p_abs(pt,eta):
    return pt/np.sin(theta(eta))

def pz(pt, eta):
    return pt/np.tan(theta(eta))

def px(pt, phi):
    return pt*np.cos(phi)

def py(pt, phi):
    return pt*np.sin(phi)

def p(pt, eta, phi):
    return np.array([px(pt, phi), py(pt, phi), pz(pt, eta)])

def inv_mass(E, p):
    return np.sqrt(E**2-np.dot(p,p))

# Given jets in an event, returns list of all combinations of at least 1 b-jet and 2 other jets
# Combinations are given as a list of indices, with the first index always the index of a known b-jet
# All combinations are unique, does not distinguish between b-jets originating from W or not
def jet_combinations(nJets, jet_b):
    jet_indices = list(range(nJets))
    other_jet_indices = jet_indices
    bjet_indices = []
    all_combs = []
    for i in jet_indices:
        if jet_b[i] == chr(1):
            bjet_indices.append(i)
   
    for b in bjet_indices:
        other_jet_indices.remove(b) # removes current b-jet from list of other jets, so next combinations are unique
        combs = it.combinations(other_jet_indices, 2)
        combs = [list(t) for t in combs]
        for comb in combs:
            comb.insert(0,b)
        all_combs += combs
    return all_combs

# Given jets in an event, returns list of all permutations of 1 b-jet and 2 W-boson jets
# Permutations are given as a list of indices
# First index per permutation is always the index of the non-W-boson b-jet 
def jet_permutations(nJets, jet_b):
    jet_indices = list(range(nJets))
    W_jet_indices = jet_indices
    bjet_indices = []
    all_perms = []

    # Make list of all indices of b-jets
    for i in jet_indices:
        if jet_b[i] == chr(1):
            bjet_indices.append(i)
    
    for b in bjet_indices: # Go through list of b-jets
        W_jet_indices.remove(b) # Remove current b-jet from list of possible W jets
        perms = it.combinations(W_jet_indices, 2) # Find all combinations of possible W jets
        perms = [np.array(list(t)) for t in perms] # Cast combinations from tuples to lists to arrays
        for comb in perms:
            comb.insert(0,b)    # Insert known b-jet indices into permutations
        all_perms += perms # Add permutations with current b-jet to list of all permutations in event
        W_jet_indices = jet_indices # Reset list of possible W jets to all jets
    return np.array(all_perms)


def jet_system_inv_mass(indices, jet_pt, jet_phi, jet_eta, jet_e):
    E_sum = 0
    p_sum = np.array([0.0,0.0,0.0])
    for j in indices:
        E_sum += jet_e[j]
        p_sum += p(jet_pt[j], jet_eta[j], jet_phi[j])
    return inv_mass(E_sum, p_sum)

def chi_squared_NO_W(indices, jet_pt, jet_phi, jet_eta, jet_e):
    m_bjj = jet_system_inv_mass(indices, jet_pt, jet_phi, jet_eta, jet_e)/1000
    return ((m_bjj - m_t)/sigma_t)**2

def chi_squared(indices, jet_pt, jet_phi, jet_eta, jet_e):
    all_jets = indices
    W_jets = indices[1:]

    m_jj = jet_system_inv_mass(W_jets, jet_pt, jet_phi, jet_eta, jet_e)/1000
    m_bjj = jet_system_inv_mass(all_jets, jet_pt, jet_phi, jet_eta, jet_e)/1000

    return ((m_jj - m_W)/sigma_W)**2 + ((m_bjj - m_t)/sigma_t)**2

def correct_triplets(event):
    correct_systems = []
    for i in range(4)
        if event.parton_top_isHadronic[i] == 1: 
            if (event.b_recoj_index[i] != -1 and event.wd1_recoj_index[i] != -1 and event.wd2_recoj_index[i] != -1):
                sys = np.array([event.b_recoj_index[i], event.wd1_recoj_index[i], event.wd2_recoj_index[i]])
                correct_systems.append(sys)
    return np.array(correct_systems)



#endregion






# Open the original ROOT file and get the TTree
original_file = ROOT.TFile.Open("ntuples/user.dreiter.38180875._000001.output.root")
original_tree = original_file.Get("reco")

# Create a new ROOT file and clone the original TTree structure
new_file = ROOT.TFile("ntuples/tttt_412043_mc20a_fastsim_hreco.root", "RECREATE")
new_tree = original_tree.CloneTree(0)  # Clone structure only, no data yet

# Define the new variable and branch
jet_triplets_bEff_85_NOSYS = np.array([])
jet_triplets_bEff_85 = new_tree.Branch('jet_triplets_bEff_85', jet_triplets_bEff_85_NOSYS, 'jet_triplets_bEff_85/F')

# Loop over the entries of the original TTree
n_entries = original_tree.GetEntries()
for event in original_tree:    

    # Calculate the new variable value (example: new variable is double of a branch value)
    jet_triplets_bEff_85_NOSYS[0] = jet_permutations(event.jet_e.size(), event.jet_GN2v01_FixedCutBEff_85_select)
    matched_jet_triplets_NOSYS[0] = correct_triplets(event)

    # Fill the new TTree with the existing data and the new variable
    new_tree.Fill()

# Write the new TTree to the new ROOT file
new_file.Write()
new_file.Close()

# Close the original file
original_file.Close()
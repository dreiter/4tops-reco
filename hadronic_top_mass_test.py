import ROOT
import numpy as np
import itertools as it
import csv

#setup
canvas = ROOT.TCanvas("canvas", "output", 1800, 1200)
f = ROOT.TFile.Open("ntuples/user.dreiter.38180875._000001.output.root")
tree = f.Get("reco")
tree.Print()

num_combinations = 0


#Set up all_mass histo
all_3_jets_hist = ROOT.TH1F("all_3_jets_hist", "Invaritant Mass of all 3 Jet Combinations [tttt_412043_mc20a_fastsim]", 100, 10, 10)
all_3_jets_hist.GetXaxis().SetTitle("M [GeV]")
all_3_jets_hist.GetYaxis().SetTitle("Combinations")

#Set up all_mass_tuth histo
all_3_jets_truth_hist = ROOT.TH1F("all_3_jets_truth_hist", "Invaritant Mass of all 4 Jet Combinations (Truth) [tttt_412043_mc20a_fastsim]", 100, 10, 10)
all_3_jets_truth_hist.GetXaxis().SetTitle("M [GeV]")
all_3_jets_truth_hist.GetYaxis().SetTitle("Combinations")

#Set up all_mass max histo
all_3_jets_max_hist = ROOT.TH1F("all_3_jets_max_hist", "Max Invaritant Mass of all 3 Jet Combinations [tttt_412043_mc20a_fastsim]", 100, 10, 10)
all_3_jets_max_hist.GetXaxis().SetTitle("M [GeV]")
all_3_jets_max_hist.GetYaxis().SetTitle("Event")

#Set up all_mass histo
all_3_jets_with_b_hist = ROOT.TH1F("all_3_jets_with_b_hist", "Invaritant Mass of all 2 Jet + 1 B-Jet Combinations [tttt_412043_mc20a_fastsim]", 100, 10, 10)
all_3_jets_with_b_hist.GetXaxis().SetTitle("M [GeV]")
all_3_jets_with_b_hist.GetYaxis().SetTitle("Combinations")

#Set up all_mass histo
all_3_jets_with_b_hist_signal = ROOT.TH1F("all_3_jets_with_b_hist_signal", "Invaritant Mass of 2 Jet + 1 B-Jet Combinations (100 MeV < M < 250 MeV) [tttt_412043_mc20a_fastsim]", 100, 10, 10)
all_3_jets_with_b_hist_signal.GetXaxis().SetTitle("M [GeV]")
all_3_jets_with_b_hist_signal.GetYaxis().SetTitle("Combinations")


#Functions

def theta(eta):
    return 2*np.arctan(np.exp(-eta))

def p_abs(pt,eta):
    return pt/np.sin(theta(eta))

def pz(pt, eta):
    return pt/np.tan(theta(eta))

def px(pt, phi):
    return pt*np.cos(phi)

def py(pt, phi):
    return pt*np.sin(phi)

def p(pt, eta, phi):
    return np.array([px(pt, phi), py(pt, phi), pz(pt, eta)])

def inv_mass(E, p):
    return np.sqrt(E**2-np.dot(p,p))

def all_three_jets_inv_mass(nJets, jet_pt, jet_phi, jet_eta, jet_e):
    mass = []
    combs = list(it.combinations(range(njets), 3))
    for comb in combs:
        E_sum = 0
        p_sum = np.array([0.0,0.0,0.0])
        for j in comb:
            E_sum += jet_e[j]
            p_sum += p(jet_pt[j], jet_eta[j], jet_phi[j])
        mass.append(inv_mass(E_sum, p_sum))
    return mass

def all_three_jets_with_b(nJets, jet_b):
    jet_indices = list(range(nJets))
    other_jet_indices = jet_indices
    bjet_indices = []
    all_combs = []
    for i in jet_indices:
        if jet_b[i] == chr(1):
            bjet_indices.append(i)
   
    for b in bjet_indices:
        other_jet_indices.remove(b)
        combs = it.combinations(other_jet_indices, 2)
        combs = [list(t) for t in combs]
        for comb in combs:
            comb.insert(0,b)
        all_combs += combs
    return all_combs


def jet_comb_inv_mass(indices, jet_pt, jet_phi, jet_eta, jet_e):
    E_sum = 0
    p_sum = np.array([0.0,0.0,0.0])
    for j in indices:
        E_sum += jet_e[j]
        p_sum += p(jet_pt[j], jet_eta[j], jet_phi[j])
    return inv_mass(E_sum, p_sum)
    
#Fill histos
i = 0
#max_i=100
max_i = 147757

data = []
num_combinations = 0
num_events = 0

for event in tree:
    if (i > max_i):
        break
    print(str(100*i/147756) + "%")
    
    nJets = event.jet_e.size()
    cuts = nJets > 2 and event.nBjets_GN2v01_85WP > 1

    if cuts:
        #jet_GN2v01_FixedCutBEff_85_select

        #all_four_jet_masses = all_four_jets_inv_mass(nJets, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e)
        #for m in all_four_jet_masses:
            #all_4_jets_hist.Fill(m/1000)

        #all_4_jets_max_hist.Fill(max(all_four_jet_masses))
        #all_four_jet_truth_masses =  all_four_jets_inv_mass(event.nJets, event.truth_jet_pt, event.truth_jet_phi, event.truth_jet_eta, event.truth_jet_e)
        #for m in all_four_jet_truth_masses:
         #   all_4_jets_truth_hist.Fill(m/1000)
        
        jet_combs = all_three_jets_with_b(nJets, event.jet_GN2v01_FixedCutBEff_85_select)
        num_combinations += len(jet_combs)
        masses = []
        for c in jet_combs:
            m = jet_comb_inv_mass(c, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e)/1000
            masses.append(m)
            all_3_jets_with_b_hist.Fill(m)
            if m < 250 and m > 100:
                all_3_jets_with_b_hist_signal.Fill(m)
    data.append(masses)
    i+=1
    num_events += 1

#all_4_jets_hist.Draw()
#canvas.Draw()
#canvas.SaveAs("all_4_jets.png")
#canvas.Clear()

#all_4_jets_max_hist.Draw()
#canvas.Draw()
#canvas.SaveAs("all_4_jets_max.png")
#canvas.Clear()

all_3_jets_with_b_hist.Draw()
canvas.Draw()
canvas.SaveAs("all_3_jets_with_b.png")
canvas.Clear()

all_3_jets_with_b_hist_signal.Draw()
canvas.Draw()
canvas.SaveAs("all_3_jets_with_b_signal.png")
canvas.Clear()

#write to csv file
csv_path = "/eos/user/d/dreiter/4tops-new-vars/all_three_jets_with_b_mass.csv"
with open(csv_path, mode = 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerows(data)

print("Total number of events: " + str(num_events))
print("Total number of combinations: " + str(num_combinations))



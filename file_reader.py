import ROOT
import numpy as np
import itertools as it
import csv

#setup
canvas = ROOT.TCanvas("canvas", "output", 1800, 1200)
f = ROOT.TFile.Open("ntuples/user.dreiter.38180875._000001.output.root")
tree = f.Get("reco")
tree.Print()

# for event in tree:
#     print("parton_top_isHadronic " + str(event.parton_top_isHadronic))
#     print("parton_Wdecay1_pt " + str([float(u) for u in event.parton_Wdecay1_pt]))
#     print("parton_Wdecay2_pt " + str([float(u) for u in event.parton_Wdecay2_pt]))
#     print("parton_b_pt " + str([float(u) for u in event.parton_b_pt]))
#     print("jet_pt_NOSYS " + str([float(u) for u in event.jet_pt_NOSYS]))
#     print("truth_jet_pt " + str([float(u) for u in event.truth_jet_pt]))
#     print("jet_GN2v01_FixedCutBEff_90_select " + str(event.jet_GN2v01_FixedCutBEff_90_select))
#     print("wd1_truthj_index " + str(event.wd1_truthj_index))
#     print("wd1_recoj_index " + str(event.wd1_recoj_index))
#     print("wd2_truthj_index " + str(event.wd2_truthj_index))
#     print("wd2_recoj_index " + str(event.wd2_recoj_index))
#     print("b_truthj_index " + str(event.b_truthj_index))
#     print("b_recoj_index " + str(event.b_recoj_index))
#     print("parton_Wdecay1_pdgid " + str(event.parton_Wdecay1_pdgid))
#     print("parton_Wdecay2_pdgid " + str(event.parton_Wdecay2_pdgid))
#     print("truth_jet_partonid " + str(event.truth_jet_partonid))
#     print("\n\n")




import ROOT
import csv

csv_path = "/eos/user/d/dreiter/4tops-new-vars/all_three_jets_with_b_mass.csv"
data = []
top_mass = 173

#Total number of events: 147756
#Total number of combinations: 6183610

#setup
canvas = ROOT.TCanvas("canvas", "output", 1800, 1200)
all_3_jets_with_b_min_top_diff_hist = ROOT.TH1F("all_3_jets_with_b_min_top_diff_hist", "4 or Fewer Closest Invaritant Masses of 3 Jet with 1 B Jet Combinations Per Event (0 < M < 250 GeV) [tttt_412043_mc20a_fastsim]", 200, 0, 250)
all_3_jets_with_b_min_top_diff_hist.GetXaxis().SetTitle("M [GeV]")
all_3_jets_with_b_min_top_diff_hist.GetYaxis().SetTitle("Combinations")

canvas = ROOT.TCanvas("canvas", "output", 1800, 1200)
all_3_jets_with_b_min_num_possible_tops_hist = ROOT.TH1F("all_3_jets_with_b_min_num_possible_tops_hist", "Number of Potential Top Quarks per Event Based on Minimum Mass Difference of 3 Jet with 1 B Jet Combinations (0 < M < 250 GeV) [tttt_412043_mc20a_fastsim]", 5, 0, 5)
all_3_jets_with_b_min_num_possible_tops_hist.GetXaxis().SetTitle("M [GeV]")
all_3_jets_with_b_min_num_possible_tops_hist.GetYaxis().SetTitle("Combinations")


with open(csv_path, mode='r', newline='') as file:
    # Create a CSV reader object
    reader = csv.reader(file)
    
    # Iterate over each row in the CSV file
    for row in reader:
        # Append the row to the data list
        comb = [float(m) for m in row]
        good_combs = []
        data.append(comb)

        # Remove masses > 250 GeV
        for m in comb:
            if m > 250:
                comb.remove(m)
        
        while len(good_combs) < 4 and len(comb) > 0:
            diff = [abs(top_mass - m) for m in comb]
            min_diff = min(diff)
            good_combs.append(comb.pop(diff.index(min_diff)))

        for m in good_combs:
            all_3_jets_with_b_min_top_diff_hist.Fill(m)
        all_3_jets_with_b_min_num_possible_tops_hist.Fill(len(good_combs))

all_3_jets_with_b_min_top_diff_hist.Draw()
canvas.Draw()
canvas.SaveAs("all_3_jets_with_b_min_top_diff.png")
canvas.Clear()

all_3_jets_with_b_min_num_possible_tops_hist.Draw()
canvas.Draw()
canvas.SaveAs("all_3_jets_with_b_min_num_possible_tops.png")
import ROOT
import itertools as it
import numpy as np

# Setup 
#region

# Setup ROOT
canvas = ROOT.TCanvas("canvas", "output", 1800, 1200)
f = ROOT.TFile.Open("ntuples/user.dreiter.38180875._000001.output.root")
tree = f.Get("reco")
#tree.Print()

# Set up results + stats

m_t = 172.6 #GeV
m_W = 80.27 #GeV
sigma_t = 2.787 #GeV
sigma_W = 4.372 #GeV

info = {
    "total events": 0,
    #"total jet combs": 0,
    "total jet perms": 0,
    "max chi-squared": 100,
    "total recos": 0,
    "total rejects": 0,
    "total correct recos": 0,
    "total correct rejects": 0,
    "total incorrect rejects": 0,
    "total incorrect recos": 0,
    "total impossible recos": 0,
    "total possible recos": 0,
    "total tops": 0,
    "total rejects": 0,
    "reco accuracy": 0.00,
    "reco possibility": 0.00,
    "reco success": 0.00,
    "top reco rate": 0.00
}

# Set up histograms

#Set up all systems mass histo 
correct_recos_mass_hist = ROOT.TH1F("correct_recos_mass_hist", "Invariant Mass of Correctly Reconstructed Tops [tttt_412043_mc20a_fastsim]", 100, 10, 10)
correct_recos_mass_hist.GetXaxis().SetTitle("M [GeV]")
correct_recos_mass_hist.GetYaxis().SetTitle("Recos")

#Set up num num_correct_recos_hist histo
num_possible_recos_hist = ROOT.TH1F("num_correct_recos_hist", "Number of Possible Hadronic Top Reconstructions [tttt_412043_mc20a_fastsim]", 100, 10, 10)
num_possible_recos_hist.GetXaxis().SetTitle("N")
num_possible_recos_hist.GetYaxis().SetTitle("Events")

#Set up num num_impossible_recos_hist histo
num_impossible_recos_hist = ROOT.TH1F("num_impossible_recos_hist", "Number of Impossible Hadronic Top Reconstructions [tttt_412043_mc20a_fastsim]", 100, 10, 10)
num_impossible_recos_hist.GetXaxis().SetTitle("N")
num_impossible_recos_hist.GetYaxis().SetTitle("Events")

#Set up chi_squared histo
chi_squared_correct_hist = ROOT.TH1F("chi_squared_correct_hist", "#chi^{2} of Correctly Reconstructed Top Decays [tttt_412043_mc20a_fastsim]", 100, 10, 10)
chi_squared_correct_hist.GetXaxis().SetTitle("#chi^{2}")
chi_squared_correct_hist.GetYaxis().SetTitle("Recos")

#endregion


# Helper Functions 
#region

def theta(eta):
    return 2*np.arctan(np.exp(-eta))

def p_abs(pt,eta):
    return pt/np.sin(theta(eta))

def pz(pt, eta):
    return pt/np.tan(theta(eta))

def px(pt, phi):
    return pt*np.cos(phi)

def py(pt, phi):
    return pt*np.sin(phi)

def p(pt, eta, phi):
    return np.array([px(pt, phi), py(pt, phi), pz(pt, eta)])

def inv_mass(E, p):
    return np.sqrt(E**2-np.dot(p,p))

# Given jets in an event, returns list of all combinations of at least 1 b-jet and 2 other jets
# Combinations are given as a list of indices, with the first index always the index of a known b-jet
# All combinations are unique, does not distinguish between b-jets originating from W or not
def jet_combinations(nJets, jet_b):
    jet_indices = list(range(nJets))
    other_jet_indices = jet_indices
    bjet_indices = []
    all_combs = []
    for i in jet_indices:
        if jet_b[i] == chr(1):
            bjet_indices.append(i)
   
    for b in bjet_indices:
        other_jet_indices.remove(b) # removes current b-jet from list of other jets, so next combinations are unique
        combs = it.combinations(other_jet_indices, 2)
        combs = [list(t) for t in combs]
        for comb in combs:
            comb.insert(0,b)
        all_combs += combs
    return all_combs

# Given jets in an event, returns list of all permutations of 1 b-jet and 2 W-boson jets
# Permutations are given as a list of indices
# First index per permutation is always the index of the non-W-boson b-jet 
def jet_permutations(nJets, jet_b):
    jet_indices = list(range(nJets))
    W_jet_indices = jet_indices
    bjet_indices = []
    all_perms = []

    # Make list of all indices of b-jets
    for i in jet_indices:
        if jet_b[i] == chr(1):
            bjet_indices.append(i)
    
    for b in bjet_indices: # Go through list of b-jets
        W_jet_indices.remove(b) # Remove current b-jet from list of possible W jets
        perms = it.combinations(W_jet_indices, 2) # Find all combinations of possible W jets
        perms = [list(t) for t in perms] # Cast combinations from tuples to lists
        for comb in perms:
            comb.insert(0,b)    # Insert known b-jet indices into permutations
        all_perms += perms # Add permutations with current b-jet to list of all permutations in event
        W_jet_indices = jet_indices # Reset list of possible W jets to all jets
    return all_perms


def jet_system_inv_mass(indices, jet_pt, jet_phi, jet_eta, jet_e):
    #vector_sum = ROOT.Math.PtEtaPhiEVector()
    #vector_sum.SetCoordinates(0,0,0,0)
    E_sum = 0
    p_sum = np.array([0.0,0.0,0.0])
    for j in indices:
        E_sum += jet_e[j]
        p_sum += p(jet_pt[j], jet_eta[j], jet_phi[j])
        #jet_vector = ROOT.Math.PtEtaPhiEVector()
        #jet_vector.SetCoordiantes(jet_pt[j], jet_eta[j], jet_phi[j], jet_e[j])
        #vector_sum += jet_vector
    return inv_mass(E_sum, p_sum)
    #return vector_sum.M()/1000

def chi_squared_NO_W(indices, jet_pt, jet_phi, jet_eta, jet_e):
    m_bjj = jet_system_inv_mass(indices, jet_pt, jet_phi, jet_eta, jet_e)/1000
    return ((m_bjj - m_t)/sigma_t)**2

def chi_squared(indices, jet_pt, jet_phi, jet_eta, jet_e):
    all_jets = indices
    W_jets = indices[1:]

    m_jj = jet_system_inv_mass(W_jets, jet_pt, jet_phi, jet_eta, jet_e)/1000
    m_bjj = jet_system_inv_mass(all_jets, jet_pt, jet_phi, jet_eta, jet_e)/1000

    return ((m_jj - m_W)/sigma_W)**2 + ((m_bjj - m_t)/sigma_t)**2

def true_chi_squared(top_mass, W_mass):
    return ((W_mass - m_W)/sigma_W)**2 + ((top_mass - m_t)/sigma_t)**2

# def smallest_chi_squared_indices(jet_systems, jet_pt, jet_phi, jet_eta, jet_e):
#     x2_indices = []
#     for s in range(len(jet_systems)):
#         x2 = chi_squared(jet_systems[s], jet_pt, jet_phi, jet_eta, jet_e)
#         if (x2 < info["max chi-squared"]):
#         x2_indices.append(s)
#     if len(x2s) < 4:
#          return


#endregion

# Computation 
#region

events_run = 0
max_run = 47756

for event in tree:
    # Limit run and log run status
    if (events_run > max_run):
        break
    print(str(100*events_run/max_run) + "% Complete")
    
    # Limit number of jets to jets with complete data
    n_jets = event.jet_e.size()

    # Set event selection
    cuts = n_jets > 2 and event.nBjets_GN2v01_85WP > 1

    #num_passes = 0
    #num_rejects = 0
    num_true_tops = 0
    num_possible_tops = 0
    num_impossible_tops = 0
    # num_correct_recos = 0
    # num_incorrect_recos = 0
    # num_missed_recos = 0

    if cuts:
        # # Find all permutations of jets in event
        # jet_systems = jet_permutations(n_jets, event.jet_GN2v01_FixedCutBEff_85_select)
        # # Fill all_perms_mass_hist

        # # Fill all chi-squared passing
        # passed_chis = []
        # passed_systems = []
        # rejected_chis = []
        # rejected_systems = []
        correct_systems = []

        for i in range(len(event.parton_top_m)):
            if event.parton_top_isHadronic[i] == 1:
                # top_mass = event.parton_top_m[i]/1000
                # W_mass = event.parton_W_m[i]/1000
                # truth_chi_squared_hist.Fill(true_chi_squared(top_mass, W_mass))
                num_true_tops += 1 

                #search for tops which can be reconstructed from reco tree data
                #store correct combinations in list
                if (event.b_recoj_index[i] != -1 and event.wd1_recoj_index[i] != -1 and event.wd2_recoj_index[i] != -1):
                    num_possible_tops += 1
                    sys = [event.b_recoj_index[i], event.wd1_recoj_index[i], event.wd2_recoj_index[i]]

                    #correct_recos_mass_hist.Fill(jet_system_inv_mass(sys, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e))
                    chi_squared_correct_hist.Fill(chi_squared(sys, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e))

                    correct_systems.append(sys)
                    # correct_systems.append([event.b_recoj_index[i], event.wd2_recoj_index[i], event.wd1_recoj_index[i]])
                else:
                    num_impossible_tops += 1

        num_possible_recos_hist.Fill(num_possible_tops)
        num_impossible_recos_hist.Fill(num_impossible_tops)

        # for sys in jet_systems:
        #     sys_mass = jet_system_inv_mass(sys, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e)/1000
        #     all_perms_mass_hist.Fill(sys_mass)

        #     sys_chi_squared = chi_squared(sys, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e)
        #     chi_squared_hist.Fill(sys_chi_squared)

            

            #print(sys_chi_squared)

            # if sys_chi_squared < info["max chi-squared"]:
            #     passed_chis.append(sys_chi_squared)
            #     passed_systems.append(sys)
            # else:
            #     rejected_chis.append(sys_chi_squared)
            #     rejected_systems.append(sys)

        # print("original passed chis: " + str(passed_chis))
        # print("original passed masses: " + str(passed_masses))
        # print("original rejected chis: " + str(rejected_chis))
        # print("original rejected masses: " + str(rejected_masses))


        #Cut out jet systems so only max 2 lowest chi-squared values remain
        # new_passed_chis = []
        # new_passed_systems = []
        
        # while len(new_passed_systems) < 2:
        #     min_chi = min(passed_chis)
        #     min_chi_index = passed_chis.index(min_chi)
        #     new_passed_chis.append(passed_chis.pop(min_chi_index))
        #     new_passed_systems.append(passed_systems.pop(min_chi_index))
        
        # rejected_chis += passed_chis
        # rejected_systems += passed_systems
        
        # passed_chis = new_passed_chis
        # passed_systems = new_passed_systems

        # num_passes = len(passed_chis)
        # num_rejects = len(rejected_chis)

        # print("cut passed chis: " + str(passed_chis))
        # print("cut passed masses: " + str(passed_masses))
        # print("cut rejected chis: " + str(rejected_chis))
        # print("cut rejected masses: " + str(rejected_masses))

        # for sys in passed_systems:
        #     # m = jet_system_inv_mass(sys, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e)/1000
        #     # chi_pass_mass_hist.Fill(m)
        #     info["total recos"] += 1
        #     if sys in correct_systems:
        #         num_correct_recos += 1
        #     else:
        #         num_incorrect_recos += 1

        # for sys in rejected_systems:
        #     # m = jet_system_inv_mass(sys, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e)/1000
        #     # chi_reject_mass_hist.Fill(m)
        #     if sys in correct_systems:
        #         num_missed_recos += 1
        #     info["total rejects"] += 1

        # num_sys_hist.Fill(len(jet_systems))
        # num_passes_hist.Fill(num_passes)
        # num_rejects_hist.Fill(num_rejects)

        #info["total jet perms"] += len(jet_systems)
        info["total tops"] += num_true_tops
        info["total possible recos"] += num_possible_tops
        info["total impossible recos"] += num_impossible_tops

    events_run += 1
    info["total events"] = events_run
    

#endregion

# Save Results
#region

info["reco possibility"] = info["total possible recos"] / info["total tops"]

# # Draw and Save Histos
# all_perms_mass_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("all_perms_mass_hist_min30.png")
# canvas.Clear()

# chi_pass_mass_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("chi_pass_mass_hist_min100.png")
# canvas.Clear()

# chi_reject_mass_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("chi_reject_mass_hist_min100.png")
# canvas.Clear()

# num_sys_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("num_sys_hist2.png")
# canvas.Clear()

# num_passes_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("num_passes_hist_min100.png")
# canvas.Clear()

# num_rejects_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("num_rejects_hist_min100.png")
# canvas.Clear()

# chi_squared_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("chi_squared_hist_min30.png")
# canvas.Clear()

# top_mass_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("top_mass_hist2.png")
# canvas.Clear()

# W_mass_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("W_mass_hist2.png")
# canvas.Clear()

# truth_chi_squared_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("truth_chi_squared_hist_min30.png")
# canvas.Clear()

# correct_recos_mass_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("correct_recos_mass_hist.png")
# canvas.Clear()

chi_squared_correct_hist.Draw()
canvas.Draw()
canvas.SaveAs("chi_squared_correct_hist.png")
canvas.Clear()

# num_possible_recos_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("num_possible_recos_hist.png")
# canvas.Clear()

# num_impossible_recos_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("num_impossible_recos_hist.png")
# canvas.Clear()

# Write Results to Text File

with open('chi_squared_efficiency_test.txt', 'w') as file:
    # Write data to the file
    for key, value in info.items():
        file.write(key + ": " + str(value) + "\n")
        print(key + ": " + str(value))

#endregion


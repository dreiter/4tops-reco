total events: 47757
total jet perms: 2006616
max chi-squared: 50
total passes: 55860
total rejects: 1950756
total correct passes: 42296
total incorrect passes: 13564
total correct rejects: 1934845
total incorrect rejects: 15911
total tops: 58207
total backgrounds: 1948409
overall accuracy: 0.9853110909112656
overall acceptance: 0.0278379121864871
top acceptance: 0.7266479976635113
top rejection: 0.27335200233648876
background acceptance: 0.006961577369022623
background rejection: 0.9930384226309774
overal rejection: 0.9721620878135129

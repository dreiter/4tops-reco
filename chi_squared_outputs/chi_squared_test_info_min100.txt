total events: 47757
total jet perms: 2006616
max chi-squared: 100
total passes: 66512
total rejects: 1940104
total correct passes: 48573
total incorrect passes: 17939
total correct rejects: 1930470
total incorrect rejects: 9634
total tops: 58207
total backgrounds: 1948409
overall accuracy: 0.9862589553756175
overall acceptance: 0.03314635186802059
top acceptance: 0.834487260982356
top rejection: 0.16551273901764393
background acceptance: 0.009206999146483106
background rejection: 0.9907930008535168
overal rejection: 0.9668536481319794

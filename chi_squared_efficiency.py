import ROOT
import itertools as it
import numpy as np
import matplotlib.pyplot as plt

# Setup 
#region

# Setup matplotlib
chi_squared_values = [10]
# chi_squared_values = [10, 20] 
total_efficiency = []
matched_efficiency = []
matched_accuracy = []
miss_rate = []
mass_mean = []
mass_stdev = []

# Setup ROOT
canvas = ROOT.TCanvas("canvas", "output", 1800, 1200)
f = ROOT.TFile.Open("ntuples/user.dreiter.38180875._000001.output.root")
tree = f.Get("reco")
#tree.Print()

# Set up results + stats

m_t = 172.6 #GeV
m_W = 80.27 #GeV
sigma_t = 2.787 #GeV
sigma_W = 4.372 #GeV

# info = {
#     "total events": 0,
#     #"total jet combs": 0,
#     "total jet perms": 0,
#     "max chi-squared": 100,
#     "total recos": 0,
#     "total rejects": 0,
#     "total correct recos": 0,
#     "total correct rejects": 0,
#     "total incorrect rejects": 0,
#     "total incorrect recos": 0,
#     "total impossible recos": 0,
#     "total possible recos": 0,
#     "total tops": 0,
#     "total rejects": 0,
#     "reco accuracy": 0.00,
#     "reco possibility": 0.00,
#     "reco success": 0.00,
#     "top reco rate": 0.00
# }

# Set up histograms

# #Set up all systems mass histo 
# correct_recos_mass_hist = ROOT.TH1F("correct_recos_mass_hist", "Invariant Mass of Correctly Reconstructed Tops [tttt_412043_mc20a_fastsim]", 100, 10, 10)
# correct_recos_mass_hist.GetXaxis().SetTitle("M [GeV]")
# correct_recos_mass_hist.GetYaxis().SetTitle("Recos")

# #Set up num num_correct_recos_hist histo
# num_possible_recos_hist = ROOT.TH1F("num_correct_recos_hist", "Number of Possible Hadronic Top Reconstructions [tttt_412043_mc20a_fastsim]", 100, 10, 10)
# num_possible_recos_hist.GetXaxis().SetTitle("N")
# num_possible_recos_hist.GetYaxis().SetTitle("Events")

# #Set up num num_impossible_recos_hist histo
# num_impossible_recos_hist = ROOT.TH1F("num_impossible_recos_hist", "Number of Impossible Hadronic Top Reconstructions [tttt_412043_mc20a_fastsim]", 100, 10, 10)
# num_impossible_recos_hist.GetXaxis().SetTitle("N")
# num_impossible_recos_hist.GetYaxis().SetTitle("Events")

# #Set up chi_squared histo
# chi_squared_correct_hist = ROOT.TH1F("chi_squared_correct_hist", "#chi^{2} of Correctly Reconstructed Top Decays [tttt_412043_mc20a_fastsim]", 100, 10, 10)
# chi_squared_correct_hist.GetXaxis().SetTitle("#chi^{2}")
# chi_squared_correct_hist.GetYaxis().SetTitle("Recos")

#Set up mass histos 
mass_hists = [ROOT.TH1F("mass_hist_" + str(chi), "Invariant Mass of Reconstructed Tops (#chi^{2} < " + str(chi) + ") [tttt_412043_mc20a_fastsim]", 100, 10, 10) for chi in chi_squared_values]
incorrect_mass_hists = [ROOT.TH1F("incorrect_mass_hist_" + str(chi), "Invariant Mass of False Reconstructed Tops (#chi^{2} < " + str(chi) + ") [tttt_412043_mc20a_fastsim]", 100, 10, 10) for chi in chi_squared_values]
correct_mass_hists = [ROOT.TH1F("correct_mass_hist_" + str(chi), "Invariant Mass of Correctly Reconstructed Tops (#chi^{2} < " + str(chi) + ") [tttt_412043_mc20a_fastsim]", 100, 10, 10) for chi in chi_squared_values]

for i in range(len(mass_hists)):
    mass_hists[i].GetXaxis().SetTitle("M [GeV]")
    mass_hists[i].GetYaxis().SetTitle("recos")
    incorrect_mass_hists[i].GetXaxis().SetTitle("M [GeV]")
    incorrect_mass_hists[i].GetYaxis().SetTitle("recos")
    correct_mass_hists[i].GetXaxis().SetTitle("M [GeV]")
    correct_mass_hists[i].GetYaxis().SetTitle("recos")


#endregion


# Helper Functions 
#region

def theta(eta):
    return 2*np.arctan(np.exp(-eta))

def p_abs(pt,eta):
    return pt/np.sin(theta(eta))

def pz(pt, eta):
    return pt/np.tan(theta(eta))

def px(pt, phi):
    return pt*np.cos(phi)

def py(pt, phi):
    return pt*np.sin(phi)

def p(pt, eta, phi):
    return np.array([px(pt, phi), py(pt, phi), pz(pt, eta)])

def inv_mass(E, p):
    return np.sqrt(E**2-np.dot(p,p))

# Given jets in an event, returns list of all combinations of at least 1 b-jet and 2 other jets
# Combinations are given as a list of indices, with the first index always the index of a known b-jet
# All combinations are unique, does not distinguish between b-jets originating from W or not
def jet_combinations(nJets, jet_b):
    jet_indices = list(range(nJets))
    other_jet_indices = jet_indices
    bjet_indices = []
    all_combs = []
    for i in jet_indices:
        if jet_b[i] == chr(1):
            bjet_indices.append(i)
   
    for b in bjet_indices:
        other_jet_indices.remove(b) # removes current b-jet from list of other jets, so next combinations are unique
        combs = it.combinations(other_jet_indices, 2)
        combs = [list(t) for t in combs]
        for comb in combs:
            comb.insert(0,b)
        all_combs += combs
    return all_combs

# Given jets in an event, returns list of all permutations of 1 b-jet and 2 W-boson jets
# Permutations are given as a list of indices
# First index per permutation is always the index of the non-W-boson b-jet 
def jet_permutations(nJets, jet_b):
    jet_indices = list(range(nJets))
    W_jet_indices = jet_indices
    bjet_indices = []
    all_perms = []

    # Make list of all indices of b-jets
    for i in jet_indices:
        if jet_b[i] == chr(1):
            bjet_indices.append(i)
    
    for b in bjet_indices: # Go through list of b-jets
        W_jet_indices.remove(b) # Remove current b-jet from list of possible W jets
        perms = it.combinations(W_jet_indices, 2) # Find all combinations of possible W jets
        perms = [list(t) for t in perms] # Cast combinations from tuples to lists
        for comb in perms:
            comb.insert(0,b)    # Insert known b-jet indices into permutations
        all_perms += perms # Add permutations with current b-jet to list of all permutations in event
        W_jet_indices = jet_indices # Reset list of possible W jets to all jets
    return all_perms


def jet_system_inv_mass(indices, jet_pt, jet_phi, jet_eta, jet_e):
    #vector_sum = ROOT.Math.PtEtaPhiEVector()
    #vector_sum.SetCoordinates(0,0,0,0)
    E_sum = 0
    p_sum = np.array([0.0,0.0,0.0])
    for j in indices:
        E_sum += jet_e[j]
        p_sum += p(jet_pt[j], jet_eta[j], jet_phi[j])
        #jet_vector = ROOT.Math.PtEtaPhiEVector()
        #jet_vector.SetCoordiantes(jet_pt[j], jet_eta[j], jet_phi[j], jet_e[j])
        #vector_sum += jet_vector
    return inv_mass(E_sum, p_sum)
    #return vector_sum.M()/1000

def chi_squared_NO_W(indices, jet_pt, jet_phi, jet_eta, jet_e):
    m_bjj = jet_system_inv_mass(indices, jet_pt, jet_phi, jet_eta, jet_e)/1000
    return ((m_bjj - m_t)/sigma_t)**2

def chi_squared(indices, jet_pt, jet_phi, jet_eta, jet_e):
    all_jets = indices
    W_jets = indices[1:]

    m_jj = jet_system_inv_mass(W_jets, jet_pt, jet_phi, jet_eta, jet_e)/1000
    m_bjj = jet_system_inv_mass(all_jets, jet_pt, jet_phi, jet_eta, jet_e)/1000

    return ((m_jj - m_W)/sigma_W)**2 + ((m_bjj - m_t)/sigma_t)**2

def true_chi_squared(top_mass, W_mass):
    return ((W_mass - m_W)/sigma_W)**2 + ((top_mass - m_t)/sigma_t)**2

# def smallest_chi_squared_indices(jet_systems, jet_pt, jet_phi, jet_eta, jet_e):
#     x2_indices = []
#     for s in range(len(jet_systems)):
#         x2 = chi_squared(jet_systems[s], jet_pt, jet_phi, jet_eta, jet_e)
#         if (x2 < info["max chi-squared"]):
#         x2_indices.append(s)
#     if len(x2s) < 4:
#          return


#endregion

# Computation 
#region

events_run = 0

all_systems = []
all_masses = []
all_chis = []

for event in tree: 

    print("Finding permutations " + str((100*events_run/47756.0)/3) + "% Complete")
        
    # Limit number of jets to jets with complete data
    n_jets = event.jet_e.size()

    # Set event selection        
    cuts = n_jets > 2 and event.nBjets_GN2v01_85WP > 1

    if cuts:
        jet_systems = jet_permutations(n_jets, event.jet_GN2v01_FixedCutBEff_85_select)
        all_systems.append(jet_systems)
        events_run += 1

        #all_masses.append([jet_system_inv_mass(sys, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e)/1000 for sys in jet_systems])
        #all_chis.append([chi_squared(sys, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e) for sys in jet_systems])



for chi_run in range(len(chi_squared_values)):
    total_correct_recos = 0.0
    total_possible_recos = 0.0
    total_hadronic_tops = 0.0
    total_recos = 0.0
    
    event_number = 0
    for event in tree:
        # Limit run and log run status
        
        print("Chi-squared " + str(chi_squared_values[chi_run]) + " " + str(100*event_number/47756/3) + "% Complete")
        
        # Limit number of jets to jets with complete data
        n_jets = event.jet_e.size()

        # Set event selection
        cuts = n_jets > 2 and event.nBjets_GN2v01_85WP > 1

        num_true_tops = 0.0
        num_possible_tops = 0.0
        num_correct_recos = 0.0

        if cuts:
            # Find all permutations of jets in event
            jet_systems = all_systems[event_number]
            # Fill all_perms_mass_hist
            #print(jet_systems)
            # Fill all chi-squared passing
            passed_chis = []
            passed_systems = []
            rejected_chis = []
            rejected_systems = []
            correct_systems = []

            for i in range(len(event.parton_top_m)):
                if event.parton_top_isHadronic[i] == 1:
                    num_true_tops += 1 

                    #search for tops which can be reconstructed from reco tree data
                    #store correct combinations in list
                    if (event.b_recoj_index[i] != -1 and event.wd1_recoj_index[i] != -1 and event.wd2_recoj_index[i] != -1):
                        num_possible_tops += 1
                        correct_systems.append([event.b_recoj_index[i], event.wd1_recoj_index[i], event.wd2_recoj_index[i]])
                        correct_systems.append([event.b_recoj_index[i], event.wd2_recoj_index[i], event.wd1_recoj_index[i]])

            total_hadronic_tops += num_true_tops
            total_possible_recos += num_possible_tops

            for sys in jet_systems:
                #sys_chi_squared = all_chis[event_number][sys_index]
                #sys = all_systems[event_number][sys_index]
                sys_chi_squared = chi_squared(sys, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e)
                if sys_chi_squared < chi_squared_values[chi_run]:
                    passed_chis.append(sys_chi_squared)
                    passed_systems.append(sys)
                else:
                    rejected_chis.append(sys_chi_squared)
                    rejected_systems.append(sys)

            #Cut out jet systems so only max 2 lowest chi-squared values remain
            
            new_passed_chis = []
            new_passed_systems = []
            
            while len(new_passed_systems) < 2 and len(passed_chis) > 0:
                min_chi = min(passed_chis)
                min_chi_index = passed_chis.index(min_chi)
                new_passed_chis.append(passed_chis.pop(min_chi_index))
                new_passed_systems.append(passed_systems.pop(min_chi_index))
            
            rejected_chis += passed_chis
            rejected_systems += passed_systems
            
            passed_chis = new_passed_chis
            passed_systems = new_passed_systems

            total_recos += len(passed_systems)

            # check for correctness
            
            for sys in passed_systems:

                m = jet_system_inv_mass(sys, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e)/1000
                mass_hists[chi_run].Fill(m)

                if sys in correct_systems:
                    num_correct_recos += 1
                    correct_mass_hists[chi_run].Fill(m)
                else:
                    incorrect_mass_hists[chi_run].Fill(m)
                
            total_correct_recos += num_correct_recos
            # for sys in rejected_systems:
            #     # m = jet_system_inv_mass(sys, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e)/1000
            #     # chi_reject_mass_hist.Fill(m)
            #     if sys in correct_systems:
            #         num_missed_recos += 1
            #     info["total rejects"] += 1

            event_number += 1
    
    matched_efficiency.append(total_correct_recos/total_possible_recos)
    total_efficiency.append(total_correct_recos/total_hadronic_tops)
    matched_accuracy.append(total_correct_recos/total_recos)
    mass_mean.append(mass_hists[chi_run].GetMean(0))
    mass_stdev.append(mass_hists[chi_run].GetStdDev(0))

    mass_hists[chi_run].Draw()
    canvas.Draw()
    canvas.SaveAs("mass_hist_" + str(chi_squared_values[chi_run]) + ".png")
    canvas.Clear()
    
#endregion

# Save Results
#region

plt.plot(chi_squared_values, total_efficiency)
plt.xlabel("#chi^{2}")
plt.ylabel("Efficiency")
plt.title("Total Efficiency of Hadronic Top Reconstruction vs Chi-Squared Limit")
plt.savefig("total_efficiency.png")
plt.clf()

plt.plot(chi_squared_values, matched_efficiency)
plt.xlabel("#chi^{2}")
plt.ylabel("Efficiency")
plt.title("Efficiency of Hadronic Top Reconstruction to Truth-Matched Decays vs Chi-Squared Limit")
plt.savefig("matched_efficiency.png")
plt.clf()

plt.plot(chi_squared_values, matched_accuracy)
plt.xlabel("#chi^{2}")
plt.ylabel("Accuracy")
plt.title("Measurable Accuracy of Hadronic Top Reconstruction vs Chi-Squared Limit")
plt.savefig("matched_accuracy.png")
plt.clf()

plt.plot(chi_squared_values, mass_mean)
plt.errorbar(chi_squared_values, mass_mean, yerr = mass_stdev, fmt = 'o')
plt.xlabel("#chi^{2}")
plt.ylabel("Mass [GeV]")
plt.title("Average Invariant Mass of Reconstructed Hadronic Tops vs Chi-Squared Limit")
plt.savefig("mass.png")
plt.clf()

print("\nChi-squared values tested: " + str(chi_squared_values))
print("total_efficiency " + str(total_efficiency))
print("matched_efficiency " + str(matched_efficiency))
print("matched_accuracy " + str(matched_accuracy))

print("mass_mean " + str(mass_mean))
print("stdev " + str(mass_stdev))


#endregion


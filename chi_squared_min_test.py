import ROOT
import itertools as it
import numpy as np

# Setup 
#region

# Setup ROOT
canvas = ROOT.TCanvas("canvas", "output", 1800, 1200)
f = ROOT.TFile.Open("ntuples/user.dreiter.38180875._000001.output.root")
tree = f.Get("reco")
#tree.Print()

# Set up results + stats

m_t = 172.6 #GeV
m_W = 80.27 #GeV
sigma_t = 2.787 #GeV
sigma_W = 4.372 #GeV

info = {
    "total events": 0,
    #"total jet combs": 0,
    "total jet perms": 0,
    "max chi-squared": 100,
    "total passes": 0,
    "total rejects": 0,
    "total correct passes": 0,
    "total incorrect passes": 0,
    "total correct rejects": 0,
    "total incorrect rejects": 0,
    "total tops": 0,
    "total backgrounds": 0,
    "overall accuracy": 0.00,
    "overall acceptance": 0.00,
    "top acceptance": 0.00,
    "top rejection": 0.00,
    "background acceptance": 0.00,
    "background rejection": 0.00
}

# Set up histograms

#Set up all combinations mass histo 
#all_combinations_mass_hist = ROOT.TH1F("all_combinations_mass_hist", "Invariant Mass of all 1 B-Jet + 2 Jet Combinations [tttt_412043_mc20a_fastsim]", 200, 10, 10)
#all_combinations_mass_hist.GetXaxis().SetTitle("M [GeV]")
#all_combinations_mass_hist.GetYaxis().SetTitle("Combinations")

#Set up all systems mass histo 
all_perms_mass_hist = ROOT.TH1F("all_perms_mass_hist", "Invariant Mass of all 1 B-Jet + 2 Jet Permutations [tttt_412043_mc20a_fastsim]", 200, 10, 10)
all_perms_mass_hist.GetXaxis().SetTitle("M [GeV]")
all_perms_mass_hist.GetYaxis().SetTitle("Jet Systems")

#Set up chi-squared passing mass histo
chi_pass_mass_hist = ROOT.TH1F("chi_pass_mass_hist", "Invariant Mass of Jet Systems with #chi^{2} < " + str(info["max chi-squared"]) + " (Signal) [tttt_412043_mc20a_fastsim]", 200, 10, 10)
chi_pass_mass_hist.GetXaxis().SetTitle("M [GeV]")
chi_pass_mass_hist.GetYaxis().SetTitle("Jet Systems")

#Set up chi-squared rejected mass histo
chi_reject_mass_hist = ROOT.TH1F("chi_reject_mass_hist", "Invariant Mass of Jet Systems with #chi^{2} >= " + str(info["max chi-squared"]) + " (Background) [tttt_412043_mc20a_fastsim]", 200, 10, 10)
chi_reject_mass_hist.GetXaxis().SetTitle("M [GeV]")
chi_reject_mass_hist.GetYaxis().SetTitle("Jet Systems")

#Set up true background mass histo
true_backgnd_mass_hist = ROOT.TH1F("true_backgnd_mass_hist", "Invariant Mass of True Background Jet Systems [tttt_412043_mc20a_fastsim]", 200, 10, 10)
true_backgnd_mass_hist.GetXaxis().SetTitle("M [GeV]")
true_backgnd_mass_hist.GetYaxis().SetTitle("Jet Systems")

#Set up num passes histo
num_passes_hist = ROOT.TH1F("num_passes_hist", "Number of Jet Systems with #chi^{2} < " + str(info["max chi-squared"]) + " [tttt_412043_mc20a_fastsim]", 100, 10, 10)
num_passes_hist.GetXaxis().SetTitle("N")
num_passes_hist.GetYaxis().SetTitle("Events")

#Set up num rejects histo
num_rejects_hist = ROOT.TH1F("num_rejects_hist", "Number of Jet Systems with #chi^{2} >= " + str(info["max chi-squared"]) + " [tttt_412043_mc20a_fastsim]", 100, 10, 10)
num_rejects_hist.GetXaxis().SetTitle("N")
num_rejects_hist.GetYaxis().SetTitle("Events")

#Set up num true top quarks histo
num_true_tops_hist = ROOT.TH1F("num_true_tops_hist", "Number of True Hadronic Top Quarks [tttt_412043_mc20a_fastsim]", 21, 0, 20)
num_true_tops_hist.GetXaxis().SetTitle("N")
num_true_tops_hist.GetYaxis().SetTitle("Events")

#Set up num true background jet systems histo
num_true_backgnd_hist = ROOT.TH1F("num_true_backgnd_hist", "Number of True Background Jet Systems [tttt_412043_mc20a_fastsim]", 51, 0, 50)
num_true_backgnd_hist.GetXaxis().SetTitle("N")
num_true_backgnd_hist.GetYaxis().SetTitle("Events")

#Set up systems histo
num_sys_hist = ROOT.TH1F("num_sys_hist", "Number of Tested Jet Permutations [tttt_412043_mc20a_fastsim]", 100, 10, 10)
num_sys_hist.GetXaxis().SetTitle("N")
num_sys_hist.GetYaxis().SetTitle("Events")

#Set up chi_squared histo
chi_squared_hist = ROOT.TH1F("chi_squared_hist", "#chi^{2} of Jet Permutations [tttt_412043_mc20a_fastsim]", 100, 10, 10)
chi_squared_hist.GetXaxis().SetTitle("#chi^2")
chi_squared_hist.GetYaxis().SetTitle("Events")

#Set up true hadronic top mass histo
top_mass_hist = ROOT.TH1F("top_mass_hist", "Mass of Hadronic Top Quarks [tttt_412043_mc20a_fastsim]", 200, 10, 10)
top_mass_hist.GetXaxis().SetTitle("GeV")
top_mass_hist.GetYaxis().SetTitle("Particles")

#Set up true hadronic W boson mass histo
W_mass_hist = ROOT.TH1F("W_mass_hist", "Mass of Hadronic W Bosons [tttt_412043_mc20a_fastsim]", 200, 10, 10)
W_mass_hist.GetXaxis().SetTitle("GeV")
W_mass_hist.GetYaxis().SetTitle("Particles")

#Truth chi-squared
truth_chi_squared_hist = ROOT.TH1F("truth_chi_squared_hist", "Chi-Squared of True Top and W Masses [tttt_412043_mc20a_fastsim]", 200, 10, 10)
truth_chi_squared_hist.GetXaxis().SetTitle("#chi^2")
truth_chi_squared_hist.GetYaxis().SetTitle("Particles")

#Set up combs histo
#num_combs_hist = ROOT.TH1F("num_combs_hist", "Number of Unique Jet Combinations [tttt_412043_mc20a_fastsim]", 200, 0, 300)
#num_combs_hist.GetXaxis().SetTitle("N")
#num_combs_hist.GetYaxis().SetTitle("Events")


#endregion


# Helper Functions 
#region

def theta(eta):
    return 2*np.arctan(np.exp(-eta))

def p_abs(pt,eta):
    return pt/np.sin(theta(eta))

def pz(pt, eta):
    return pt/np.tan(theta(eta))

def px(pt, phi):
    return pt*np.cos(phi)

def py(pt, phi):
    return pt*np.sin(phi)

def p(pt, eta, phi):
    return np.array([px(pt, phi), py(pt, phi), pz(pt, eta)])

def inv_mass(E, p):
    return np.sqrt(E**2-np.dot(p,p))

# Given jets in an event, returns list of all combinations of at least 1 b-jet and 2 other jets
# Combinations are given as a list of indices, with the first index always the index of a known b-jet
# All combinations are unique, does not distinguish between b-jets originating from W or not
def jet_combinations(nJets, jet_b):
    jet_indices = list(range(nJets))
    other_jet_indices = jet_indices
    bjet_indices = []
    all_combs = []
    for i in jet_indices:
        if jet_b[i] == chr(1):
            bjet_indices.append(i)
   
    for b in bjet_indices:
        other_jet_indices.remove(b) # removes current b-jet from list of other jets, so next combinations are unique
        combs = it.combinations(other_jet_indices, 2)
        combs = [list(t) for t in combs]
        for comb in combs:
            comb.insert(0,b)
        all_combs += combs
    return all_combs

# Given jets in an event, returns list of all permutations of 1 b-jet and 2 W-boson jets
# Permutations are given as a list of indices
# First index per permutation is always the index of the non-W-boson b-jet 
def jet_permutations(nJets, jet_b):
    jet_indices = list(range(nJets))
    W_jet_indices = jet_indices
    bjet_indices = []
    all_perms = []

    # Make list of all indices of b-jets
    for i in jet_indices:
        if jet_b[i] == chr(1):
            bjet_indices.append(i)
    
    for b in bjet_indices: # Go through list of b-jets
        W_jet_indices.remove(b) # Remove current b-jet from list of possible W jets
        perms = it.combinations(W_jet_indices, 2) # Find all combinations of possible W jets
        perms = [list(t) for t in perms] # Cast combinations from tuples to lists
        for comb in perms:
            comb.insert(0,b)    # Insert known b-jet indices into permutations
        all_perms += perms # Add permutations with current b-jet to list of all permutations in event
        W_jet_indices = jet_indices # Reset list of possible W jets to all jets
    return all_perms


def jet_system_inv_mass(indices, jet_pt, jet_phi, jet_eta, jet_e):
    vector_sum = ROOT.Math.PtEtaPhiEVector()
    vector_sum.SetCoordinates(0,0,0,0)
    #E_sum = 0
    #p_sum = np.array([0.0,0.0,0.0])
    for j in indices:
        #E_sum += jet_e[j]
        #p_sum += p(jet_pt[j], jet_eta[j], jet_phi[j])
        jet_vector = ROOT.Math.PtEtaPhiEVector()
        jet_vector.SetCoordiantes(jet_pt[j], jet_eta[j], jet_phi[j], jet_e[j])
        vector_sum += jet_vector
    #return inv_mass(E_sum, p_sum)
    return vector_sum.M()/1000

def chi_squared_NO_W(indices, jet_pt, jet_phi, jet_eta, jet_e):
    m_bjj = jet_system_inv_mass(indices, jet_pt, jet_phi, jet_eta, jet_e)/1000
    return ((m_bjj - m_t)/sigma_t)**2

def chi_squared(indices, jet_pt, jet_phi, jet_eta, jet_e):
    all_jets = indices
    W_jets = indices[1:]

    m_jj = jet_system_inv_mass(W_jets, jet_pt, jet_phi, jet_eta, jet_e)/1000
    m_bjj = jet_system_inv_mass(all_jets, jet_pt, jet_phi, jet_eta, jet_e)/1000

    return ((m_jj - m_W)/sigma_W)**2 + ((m_bjj - m_t)/sigma_t)**2

def true_chi_squared(top_mass, W_mass):
    return ((W_mass - m_W)/sigma_W)**2 + ((top_mass - m_t)/sigma_t)**2

# def smallest_chi_squared_indices(jet_systems, jet_pt, jet_phi, jet_eta, jet_e):
#     x2_indices = []
#     for s in range(len(jet_systems)):
#         x2 = chi_squared(jet_systems[s], jet_pt, jet_phi, jet_eta, jet_e)
#         if (x2 < info["max chi-squared"]):
#         x2_indices.append(s)
#     if len(x2s) < 4:
#          return


#endregion

# Computation 
#region

events_run = 0
max_run = 47756

for event in tree:
    # Limit run and log run status
    if (events_run > max_run):
        break
    print(str(100*events_run/max_run) + "% Complete")
    
    # Limit number of jets to jets with complete data
    n_jets = event.jet_e.size()

    # Set event selection
    cuts = n_jets > 2 and event.nBjets_GN2v01_85WP > 1

    num_passes = 0
    num_rejects = 0
    num_true_tops = 0
    num_true_backgnd = 0

    if cuts:
        # Find all permutations of jets in event
        jet_systems = jet_permutations(n_jets, event.jet_GN2v01_FixedCutBEff_85_select)
        # Fill all_perms_mass_hist

        # Fill all chi-squared passing
        passed_chis = []
        passed_systems = []
        rejected_chis = []
        rejected_systems = []


        for sys in jet_systems:
            sys_mass = jet_system_inv_mass(sys, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e)/1000
            all_perms_mass_hist.Fill(sys_mass)

            sys_chi_squared = chi_squared(sys, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e)
            chi_squared_hist.Fill(sys_chi_squared)

            

            #print(sys_chi_squared)

            if sys_chi_squared < info["max chi-squared"]:
                passed_chis.append(sys_chi_squared)
                passed_systems.append(sys)
            else:
                rejected_chis.append(sys_chi_squared)
                rejected_systems.append(sys)

        # print("original passed chis: " + str(passed_chis))
        # print("original passed masses: " + str(passed_masses))
        # print("original rejected chis: " + str(rejected_chis))
        # print("original rejected masses: " + str(rejected_masses))


        #Cut out jet systems so only max 2 lowest chi-squared values remain
        new_passed_chis = []
        new_passed_systems = []
        
        while len(new_passed_systems) < 2:
            min_chi = min(passed_chis)
            min_chi_index = passed_chis.index(min_chi)
            new_passed_chis.append(passed_chis.pop(min_chi_index))
            new_passed_systems.append(passed_systems.pop(min_chi_index))
        
        rejected_chis += passed_chis
        rejected_systems += passed_systems
        
        passed_chis = new_passed_chis
        passed_systems = new_passed_systems

        num_passes = len(passed_chis)
        num_rejects = len(rejected_chis)

        # print("cut passed chis: " + str(passed_chis))
        # print("cut passed masses: " + str(passed_masses))
        # print("cut rejected chis: " + str(rejected_chis))
        # print("cut rejected masses: " + str(rejected_masses))

        for sys in passed_systems:
            m = jet_system_inv_mass(sys, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e)/1000
            chi_pass_mass_hist.Fill(m)
            info["total passes"] += 1

        for sys in rejected_systems:
            m = jet_system_inv_mass(sys, event.jet_pt_NOSYS, event.jet_phi, event.jet_eta, event.jet_e)/1000
            chi_reject_mass_hist.Fill(m)
            info["total rejects"] += 1


        for i in range(len(event.parton_top_m)):
            if event.parton_top_isHadronic[i] == 1:
                top_mass = event.parton_top_m[i]/1000
                W_mass = event.parton_W_m[i]/1000
                top_mass_hist.Fill(top_mass)
                W_mass_hist.Fill(W_mass)
                truth_chi_squared_hist.Fill(true_chi_squared(top_mass, W_mass))
                num_true_tops += 1

        

        num_true_backgnd = len(jet_systems) - num_true_tops

        if num_passes == num_true_tops:
            info["total correct passes"] += num_passes
            info["total correct rejects"] += num_rejects
        elif num_passes < num_true_tops:
            info["total correct passes"] += num_passes
            info["total incorrect rejects"] += num_true_tops - num_passes
            info["total correct rejects"] += num_rejects - (num_true_tops - num_passes)
        else:
            info["total correct rejects"] += num_rejects
            info["total incorrect passes"] += num_passes - num_true_tops
            info["total correct passes"] += num_true_tops

        num_sys_hist.Fill(len(jet_systems))
        num_passes_hist.Fill(num_passes)
        num_rejects_hist.Fill(num_rejects)

        info["total jet perms"] += len(jet_systems)
        info["total tops"] += num_true_tops
        info["total backgrounds"] += num_true_backgnd

    events_run += 1
    info["total events"] = events_run
    

#endregion

# Save Results
#region

info["overall accuracy"] = (info["total correct passes"] + info["total correct rejects"]) / (info["total passes"] + info["total rejects"])
info["overall acceptance"] = (info["total passes"]) / (info["total passes"] + info["total rejects"])
info["overal rejection"] = (info["total rejects"])/ (info["total passes"] + info["total rejects"])
info["top acceptance"] = info["total correct passes"]/info["total tops"]
info["top rejection"] = info["total incorrect rejects"]/info["total tops"]
info["background acceptance"] = info["total incorrect passes"]/info["total backgrounds"]
info["background rejection"] = info["total correct rejects"]/info["total backgrounds"]

# # Draw and Save Histos
# all_perms_mass_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("all_perms_mass_hist_min30.png")
# canvas.Clear()

chi_pass_mass_hist.Draw()
canvas.Draw()
canvas.SaveAs("chi_pass_mass_hist_min100.png")
canvas.Clear()

chi_reject_mass_hist.Draw()
canvas.Draw()
canvas.SaveAs("chi_reject_mass_hist_min100.png")
canvas.Clear()

# num_sys_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("num_sys_hist2.png")
# canvas.Clear()

num_passes_hist.Draw()
canvas.Draw()
canvas.SaveAs("num_passes_hist_min100.png")
canvas.Clear()

num_rejects_hist.Draw()
canvas.Draw()
canvas.SaveAs("num_rejects_hist_min100.png")
canvas.Clear()

# chi_squared_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("chi_squared_hist_min30.png")
# canvas.Clear()

# top_mass_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("top_mass_hist2.png")
# canvas.Clear()

# W_mass_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("W_mass_hist2.png")
# canvas.Clear()

# truth_chi_squared_hist.Draw()
# canvas.Draw()
# canvas.SaveAs("truth_chi_squared_hist_min30.png")
# canvas.Clear()

# Write Results to Text File

with open('chi_squared_test_info_min100.txt', 'w') as file:
    # Write data to the file
    for key, value in info.items():
        file.write(key + ": " + str(value) + "\n")
        print(key + ": " + str(value))

#endregion


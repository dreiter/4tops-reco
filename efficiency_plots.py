import ROOT
import itertools as it
import numpy as np
import matplotlib.pyplot as plt

# Setup 
#region

# Setup matplotlib

chi_squared_values = [10, 20, 40, 60, 80, 100, 120, 140, 160, 180]
total_efficiency = [0.026957430951824787, 0.03424878477436291, 0.03845897153535556, 0.03967697800914339, 0.039943938332165384, 0.040077418493676374, 0.040138596901035585, 0.0401942136349985, 0.04020533698179108, 0.04021089865518737]
matched_efficiency = [0.417880851797568730, 0.5309078368824899, 0.5961720838003276, 0.6150530218122252, 0.6191913095956548, 0.6212604534873696, 0.6222088111044055, 0.6230709543926201, 0.623243383050263, 0.6233295973790844]
matched_accuracy = [0.06551505075490316, 0.05335435854336883, 0.04345176006333966, 0.03916036316927772, 0.036627720176865684, 0.035055969876968435, 0.034013573381091525, 0.03324638761230489, 0.03266754033169145, 0.032222410396741216]



plt.plot(chi_squared_values, total_efficiency)
plt.xlabel("Chi-Squared")
plt.ylabel("Efficiency")
plt.title("Percent of all Hadronic Tops Correctly Reconstructed")
plt.savefig("total_efficiency.png")
plt.clf()

plt.plot(chi_squared_values, matched_efficiency)
plt.xlabel("Chi-Squared")
plt.ylabel("Efficiency")
plt.title("Percent of Truth-Matched Hadronic Tops Correctly Reconstructed")
plt.savefig("matched_efficiency.png")
plt.clf()

plt.plot(chi_squared_values, matched_accuracy)
plt.xlabel("Chi-Squared")
plt.ylabel("Accuracy")
plt.title("Accuracy of Hadronic Top Reconstructions")
plt.savefig("matched_accuracy.png")
plt.clf()

# plt.plot(chi_squared_values, mass_mean)
# plt.errorbar(chi_squared_values, mass_mean, yerr = mass_stdev, fmt = 'o')
# plt.xlabel("#chi^{2}")
# plt.ylabel("Mass [GeV]")
# plt.title("Average Invariant Mass of Reconstructed Hadronic Tops vs Chi-Squared Limit")
# plt.savefig("mass.png")
# plt.clf()


#endregion

